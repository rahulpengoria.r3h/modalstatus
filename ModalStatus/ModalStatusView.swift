//
//  ModalStatusView.swift
//  ModalStatus
//
//  Created by Rahul Pengoria on 06/11/19.
//  Copyright © 2019 Blibli. All rights reserved.
//

import UIKit

class ModalStatusView: UIView {

    @IBOutlet weak var imageview: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    let nibName = "ModalStatusView"
    var contentView: UIView!
    
    public override init(frame: CGRect) {
     // For use in code
      super.init(frame: frame)
      setUpView()
    }

    public required init?(coder aDecoder: NSCoder) {
       // For use in Interface Builder
       super.init(coder: aDecoder)
      setUpView()
    }
    
    private func setUpView() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: self.nibName, bundle: bundle)
        self.contentView = nib.instantiate(withOwner: self, options: nil).first as? UIView
        addSubview(contentView)
        
        contentView.center = self.center
        contentView.autoresizingMask = []
        contentView.translatesAutoresizingMaskIntoConstraints = true
        
        label1.text = ""
        label2.text = ""
    }
    
    func set(image: UIImage) {
        self.imageview.image = image
    }
    func set(headline text: String) {
        self.label1.text = text
    }
    func set(subheading text: String) {
        self.label2.text = text
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
